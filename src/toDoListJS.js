const data = [
  {
    id: 0,
    task: "Hug 8 people",
    who: "Scott",
    dueDate: "2013-05-19",
    done: false
  },
  {
    id: 1,
    task: "Learn JS",
    who: "Elisabeth",
    dueDate: "2013-05-21",
    done: false
  },
  {
    id: 2,
    task: "Buy flowers",
    who: "Trish",
    dueDate: "2013-05-30",
    done: false
  },
  { id: 3, task: "go Gym", who: "Josh", dueDate: "2013-05-15", done: true },
  { id: 4, task: "Party", who: "Scott", dueDate: "2013-05-19", done: false },
  {
    id: 5,
    task: "Kiss my wife",
    who: "Elisabeth",
    dueDate: "2013-05-21",
    done: false
  },
  {
    id: 6,
    task: "Woof on the moon",
    who: "Trish",
    dueDate: "2013-05-30",
    done: false
  },
  { id: 7, task: "Eat my cat", who: "Josh", dueDate: "2013-05-15", done: true }
]

export default data
