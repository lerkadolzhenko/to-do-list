import React from "react"
import data from "../../../toDoListJS"
import Search from "./components/Search"
import Filter from "./components/Filter"
import SingleTask from "./components/SingleTask"
import AddTaskBtn from "./components/AddTaskModal/AddTaskBtn"
import { List, Container, withStyles, Grid } from "@material-ui/core"
import AddTaskModal from "./components/AddTaskModal"
import DetailTaskModal from "./components/DetailTaskModal"
import EditTaskModal from "./components/EditTaskModal/EditTaskModal"

const useStyles = (theme) => ({
  margin: {
    marginTop: "20px"
  }
})

class ToDoList extends React.Component {
  state = {
    tasks: data,
    currentTask: 0,
    inputTask: "",
    personName: "",
    date: "",
    checkedDone: false,
    checkedNotDone: false,
    searchText: "",
    modalAddTaskOpen: false,
    modalDetailedTaskOpen: false,
    modalEditTaskOpen: false
  }

  handleAddTaskModalOpen = () => {
    this.setState({ modalAddTaskOpen: true })
  }

  handleAddTaskModalClose = () => {
    this.setState({ modalAddTaskOpen: false })
  }

  handleDetailedTaskModalOpen = (id) => {
    console.log(id, this.state.currentTask)
    this.setState({ modalDetailedTaskOpen: true, currentTask: id })
  }

  handleDetailedTaskModalClose = () => {
    this.setState({ modalDetailedTaskOpen: false })
  }

  handleEditTaskModalOpen = (id) => {
    const { tasks } = this.state
    const toggledItem = tasks.find((task) => task.id === id)
    this.setState({
      modalEditTaskOpen: true,
      currentTask: id,
      inputTask: tasks[toggledItem].task,
      personName: tasks[toggledItem].who
    })
  }

  handleEditTaskModalClose = () => {
    this.setState({ modalEditTaskOpen: false })
  }

  handleEditBtn = () => {
    const { tasks, inputTask, personName, currentTask } = this.state
    const newData = [...tasks]
    const toggledItem = newData.findIndex((task) => task.id === currentTask)
    newData[toggledItem] = {
      ...newData[toggledItem],
      task: inputTask,
      who: personName
    }
    console.log(newData, tasks)
    this.setState({ tasks: newData })
  }

  handleInputNewTask = (name) => (event) => {
    console.log(name, event)
    this.setState({ [name]: event.target.value })
  }

  handleTaskDoneChange = (id) => {
    const { tasks } = this.state
    console.log(id)
    const newData = [...tasks]
    const toggledItem = newData.findIndex((task) => task.id === id)
    newData[toggledItem] = {
      ...newData[toggledItem],
      done: !newData[toggledItem].done
    }
    this.setState({ tasks: newData })
  }

  handleDeleteBtn = (id) => {
    const { tasks } = this.state
    const newData = [...tasks]
    const deletedItem = newData.findIndex((task) => task.id === id)
    console.log(id, deletedItem)
    newData.splice(deletedItem, 1)
    this.setState({ tasks: newData })
  }

  handleFilterChange = (name) => (event) => {
    console.log(name, event)
    this.setState({ [name]: event.target.checked })
  }

  handleSearchTaskName = (taskName) => {
    if (this.state.searchText.length === 0) {
      taskName = taskName.trim()
    }
    this.setState({
      searchText: taskName
    })
  }

  handleAddTaskBtn = () => {
    let date = new Date().toISOString().slice(0, 10)
    const { tasks, inputTask, personName } = this.state
    const newData = [...tasks]
    const newItem = {
      id: newData.length + 1,
      task: inputTask,
      who: personName,
      dueDate: date,
      done: false
    }
    newData.push(newItem)
    console.log(newItem, newData, tasks)
    this.setState({ tasks: newData })
  }

  render() {
    const { classes } = this.props
    const {
      checkedDone,
      checkedNotDone,
      searchText,
      tasks,
      inputTask,
      modalAddTaskOpen,
      modalDetailedTaskOpen,
      currentTask,
      personName,
      modalEditTaskOpen,
    } = this.state

    const filterFunc = (task) => {
      const filteredByName = task.task
        .toLowerCase()
        .includes(searchText.toLowerCase())
      if (checkedDone && !checkedNotDone) {
        return filteredByName && task.done === true
      } else if (checkedNotDone && !checkedDone) {
        return filteredByName && task.done === false
      } else {
        return filteredByName
      }
    }
    return (
      <div>
        <Container className={classes.margin} component="main" maxWidth="md">
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center">
            <Search
              searchText={searchText}
              onSearchChange={this.handleSearchTaskName}
            />
            <Filter
              checkedDone={checkedDone}
              checkedNotDone={checkedNotDone}
              onFilterChange={this.handleFilterChange}
            />
          </Grid>
          <List>
            {tasks.length > 0 &&
              tasks
                .filter((task) => filterFunc(task))
                .map((task) => (
                  <SingleTask
                    key={task.id}
                    task={task}
                    onTaskDoneChange={this.handleTaskDoneChange}
                    onTaskDelete={this.handleDeleteBtn}
                    onDetailedTask={this.handleDetailedTaskModalOpen}
                    onEditModal={this.handleEditTaskModalOpen}
                  />
                ))}
          </List>
          <AddTaskBtn
            onModalOpen={this.handleAddTaskModalOpen}
            className={classes.margin}
          />
          <AddTaskModal
            open={modalAddTaskOpen}
            inputTask={inputTask}
            personName={personName}
            onModalClose={this.handleAddTaskModalClose}
            onInputTask={this.handleInputNewTask}
            onAddTask={this.handleAddTaskBtn}
          />
          <DetailTaskModal
            task={tasks.find((task) => task.id === currentTask)}
            open={modalDetailedTaskOpen}
            onTaskDoneChange={this.handleTaskDoneChange}
            onModalClose={this.handleDetailedTaskModalClose}
          />
          <EditTaskModal
            open={modalEditTaskOpen}
            inputTask={inputTask}
            personName={personName}
            onInputTask={this.handleInputNewTask}
            onEditTask={this.handleEditBtn}
            onModalClose={this.handleEditTaskModalClose}
          />
        </Container>
      </div>
    )
  }
}
export default withStyles(useStyles)(ToDoList)
