import React from "react"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
import Checkbox from "@material-ui/core/Checkbox"
import IconButton from "@material-ui/core/IconButton"
import DeleteIcon from "@material-ui/icons/Delete"
import InfoIcon from "@material-ui/icons/Info"
import EditIcon from '@material-ui/icons/Edit';

export default function SingleTask({
  task,
  onTaskDoneChange,
  onTaskDelete,
  onDetailedTask,
  onEditModal
}) {
  const labelId = `checkbox-list-label-${task}`

  return (
    <ListItem
      key={task}
      role={undefined}
      dense
      style={{ backgroundColor: task.done === true ? "#f0f5ff" : "#fff" }}>
      <ListItemIcon>
        <Checkbox
          onClick={() => {
            onTaskDoneChange(task.id)
          }}
          edge="start"
          checked={task.done}
          tabIndex={-1}
          disableRipple
          color="primary"
          inputProps={{ "aria-labelledby": labelId }}
        />
      </ListItemIcon>
      <ListItemText id={labelId} primary={` ${task.task}`} />
      
      <IconButton aria-label="info-edit" onClick={() => onEditModal(task.id)}>
        <EditIcon />
      </IconButton>

      <IconButton aria-label="info" onClick={() => onDetailedTask(task.id)}>
        <InfoIcon />
      </IconButton>

      <IconButton
        edge="end"
        aria-label="delete"
        onClick={() => {
          onTaskDelete(task.id)
        }}>
        <DeleteIcon />
      </IconButton>
    </ListItem>
  )
}
