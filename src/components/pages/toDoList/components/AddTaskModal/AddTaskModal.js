import React from "react"
import AddTaskForm from "./AddTaskForm"
import ModalWindow from "../ModalWindow"

export const AddTaskModal = ({
  onAddTask,
  inputTask,
  onInputTask,
  onModalClose,
  open
}) => {
  return (
    <div>
      <ModalWindow open={open} onModalClose={onModalClose}>
        <AddTaskForm
          onAddTask={onAddTask}
          inputTask={inputTask}
          onInputTask={onInputTask}
          onModalClose={onModalClose}
        />
      </ModalWindow>
    </div>
  )
}

export default AddTaskModal
