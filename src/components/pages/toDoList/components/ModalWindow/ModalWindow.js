import React from "react"
import Modal from "@material-ui/core/Modal"

export const ModalWindow = ({ onModalClose, open, children }) => {
  return (
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={open}
      onClose={onModalClose}>
      {children}
    </Modal>
  )
}
export default ModalWindow
