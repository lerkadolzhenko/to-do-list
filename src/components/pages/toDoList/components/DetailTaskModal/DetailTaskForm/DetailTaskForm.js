import React from "react"
import {
  FormControl,
  makeStyles,
  Typography,
  FormControlLabel,
  Switch
} from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  paper: {
    top: "40%",
    left: "25%",
    [theme.breakpoints.down("sm")]: {
      left: "5%",
      width: "90%"
    },
    position: "absolute",
    width: "50%",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  },
  formControlInput: {
    width: "100%"
  }
}))

export const DetailTaskForm = React.forwardRef(
  ({ task, onTaskDoneChange }, ref) => {
    const classes = useStyles()

    return (
      <div className={classes.paper} ref={ref}>
        <FormControl className={classes.formControlInput}>
          <Typography color="initial" gutterBottom>
            Task : {task.task}
          </Typography>
          <Typography color="initial" gutterBottom>
            Who : {task.who}
          </Typography>
          <Typography color="initial" gutterBottom>
            Due Date : {task.dueDate}
          </Typography>
          <FormControlLabel
            control={
              <Switch
                checked={task.done}
                color="primary"
                onChange={() => {
                  onTaskDoneChange(task.id)
                }}
              />
              // onChange={handleChange('checkedA')} value="checkedA"
            }
            label={`${task.done ? "done" : "not done"}`}
          />
        </FormControl>
      </div>
    )
  }
)
export default DetailTaskForm
