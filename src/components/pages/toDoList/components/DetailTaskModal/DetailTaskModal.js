import React from "react"
import ModalWindow from "../ModalWindow"
import DetailTaskForm from "./DetailTaskForm"


export const DetailTaskModal = ({
  onModalClose,
  open,
  task,
  onTaskDoneChange
}) => {
  return (
    <div>
      <ModalWindow open={open} onModalClose={onModalClose}>
        <DetailTaskForm task={task} onTaskDoneChange={onTaskDoneChange}/>
      </ModalWindow>
    </div>
  )
}

export default DetailTaskModal
