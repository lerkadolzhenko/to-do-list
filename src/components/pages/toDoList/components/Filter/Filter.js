import React from "react"
import {
  IconButton,
  Checkbox,
  MenuItem,
  Menu,
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel
} from "@material-ui/core"
import FilterListIcon from "@material-ui/icons/FilterList"

export const Filter = ({ checkedDone, checkedNotDone, onFilterChange }) => {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="menu"
        aria-haspopup="true"
        onClick={handleClick}>
        <FilterListIcon />
      </IconButton>
      <Menu
        id="menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}>
        <MenuItem>
          <FormControl>
            <FormLabel component="legend">Pick one or two</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checkedDone}
                    onChange={onFilterChange("checkedDone")}
                    value="checkedDone"
                    color="primary"
                  />
                }
                label="completed tasks"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checkedNotDone}
                    onChange={onFilterChange("checkedNotDone")}
                    value="checkedNotDone"
                    color="primary"
                  />
                }
                label="uncompleted tasks"
              />
            </FormGroup>
          </FormControl>
        </MenuItem>
      </Menu>
    </div>
  )
}

export default Filter
