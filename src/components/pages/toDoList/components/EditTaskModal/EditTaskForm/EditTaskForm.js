import React from "react"
import Button from "@material-ui/core/Button"
import { FormControl, Input, makeStyles } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  paper: {
    top: "40%",
    left: "25%",
    [theme.breakpoints.down("sm")]: {
      left: "5%",
      width: "90%"
    },
    position: "absolute",
    width: "50%",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  },
  formControlInput: {
    width: "100%",
    marginTop: "3vh"
  },
  margin: {
    marginTop: "3vh"
  }
}))

export const EditTaskForm = React.forwardRef(
  ({ onModalClose, onInputTask, onEditTask, personName, inputTask }, ref) => {
    const classes = useStyles()

    return (
      <div className={classes.paper} ref={ref}>
        <FormControl className={classes.formControlInput}>
          <Input
            placeholder="Input task here"
            value={inputTask}
            onChange={onInputTask("inputTask")}
          />
        </FormControl>
        <FormControl className={classes.formControlInput}>
          <Input
            placeholder="Input name here"
            value={personName}
            onChange={onInputTask("personName")}
          />
        </FormControl>
        <Button
          className={classes.margin}
          variant="contained"
          color="primary"
          onClick={() => {
            onEditTask()
            onModalClose()
          }}>
          Add new task
        </Button>
      </div>
    )
  }
)

export default EditTaskForm
