import React from "react"
import ModalWindow from "../ModalWindow"
import EditTaskForm from "./EditTaskForm"

export const EditTaskModal = ({
  onModalClose,
  open,
  onInputTask,
  onEditTask,
  inputTask,
  personName
}) => {
  return (
    <div>
      <ModalWindow open={open} onModalClose={onModalClose}>
        <EditTaskForm
          onModalClose={onModalClose}
          onInputTask={onInputTask}
          inputTask={inputTask}
          personName={personName}
          onEditTask={onEditTask}
        />
      </ModalWindow>
    </div>
  )
}

export default EditTaskModal
