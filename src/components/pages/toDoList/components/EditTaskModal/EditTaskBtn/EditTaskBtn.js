import React from "react"
import Button from "@material-ui/core/Button"
import AddIcon from "@material-ui/icons/Add"

export const EditTaskBtn = ({ onModalOpen }) => {
  return (
    <div>
      <Button variant="contained" color="primary" onClick={onModalOpen}>
        Add new task
        <AddIcon />
      </Button>
    </div>
  )
}

export default EditTaskBtn
