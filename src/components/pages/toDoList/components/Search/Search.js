import React from "react"
import {
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  makeStyles
} from "@material-ui/core"
import SearchIcon from "@material-ui/icons/Search"

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1)
  }
}))

export const Search = ({ onSearchChange, searchText }) => {
  const classes = useStyles()
  return (
    <FormControl className={classes.margin}>
      <InputLabel htmlFor="input-with-icon-adornment">Task Name</InputLabel>
      <Input
        id="input-with-icon-adornment"
        onChange={(event) => {
          onSearchChange(event.target.value)
        }}
        value={searchText}
        placeholder="Name"
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        }
      />
    </FormControl>
  )
}

export default Search
